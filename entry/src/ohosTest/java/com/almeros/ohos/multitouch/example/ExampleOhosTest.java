/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package com.almeros.ohos.multitouch.example;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * 测试
 *
 * @author ljx
 * @since 2021-05-08
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.almeros.ohos.multitouch.example", actualBundleName);
    }

    @Test
    public void getX() {
        try {
            Class  mainAbilitySlice = Class.forName("com.almeros.ohos.multitouch.library.MoveGestureDetector");
            Method log = mainAbilitySlice.getMethod("getX");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getY() {
        try {
            Class  mainAbilitySlice = Class.forName("com.almeros.ohos.multitouch.library.MoveGestureDetector");
            Method log = mainAbilitySlice.getMethod("getY");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAngle() {
        try {
            Class  mainAbilitySlice = Class.forName("com.almeros.ohos.multitouch.library.RotateGestureDetector");
            Method log = mainAbilitySlice.getMethod("getAngle");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getScale() {
        try {
            Class  mainAbilitySlice = Class.forName("com.almeros.ohos.multitouch.library.ScaleGestureDetector");
            Method log = mainAbilitySlice.getMethod("getScale");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getShovePixelsDelta() {
        try {
            Class  mainAbilitySlice = Class.forName("com.almeros.ohos.multitouch.library.ShoveGestureDetector");
            Method log = mainAbilitySlice.getMethod("getShovePixelsDelta");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}