# ohos-gesture-detectors

#### 项目介绍
- 项目名称：手势检测器框架
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现各种手势检测功能
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本： Release Tags 1.0.1

#### 效果演示

<img src="img/icon.gif"></img>

#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ohos-gesture-detectors:0.0.2-SNAPSHOT')
    ......  
 }
```

#### 使用说明

组件主要基于ScaleGestureDetector功能，增加手势检测器扩展框架。

项目中封装有各种手势检测框架，使用中需要根据接收的 onTouchEvent，侦测由多个触点（多点触控）引发的变形手势。callback 方法XXXGestureDetector.OnScaleGestureListener会在特定手势事件发生时通知用户。该类仅能和Touch事件引发的 TouchEvent配合使用。

使用该类，需要为你的Component创建XXXGestureDetector实例。

```
mRotateDetector = new RotateGestureDetector(new RotateListener());
```

确保在 onTouchEvent(Component component, TouchEvent event)方法中调用onTouchEvent (MotionEvent)。

```
@Override
public boolean onTouchEvent(Component component, TouchEvent event) {
    mRotateDetector.onTouchEvent(event);
    }
```

RotateGestureDetector（旋转手势检测）

```
RotateListener extends RotateGestureDetector.SimpleOnRotationGestureListener {

    @Override
    public boolean onRotation(RotateGestureDetector rotationDetector) {
        // 处理功能
        return false;
    }
}
```

ScaleGestureDetector（缩放手势检测）

```
ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

    @Override
    public boolean onScale(ScaleGestureDetector scaleDetector) {
        // 处理功能
        return false;
    }
}
```

MoveGestureDetector（滑动手势检测）

```
MoveListener extends MoveGestureDetector.SimpleOnScaleGestureListener {

    @Override
    public boolean onMove(MoveGestureDetector detector) {
        // 处理功能
        return false;
    }
}
```

ShoveGestureDetector（推动手势检测）

```
ShoveListener extends ShoveGestureDetector.SimpleOnShoveGestureListener {

    @Override
    public boolean onShove(ShoveGestureDetector detector) {
        // 处理功能
        return false;
    }
}
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.2-SNAPSHOT

#### 版权和许可信息

This project is licensed with the 2-clause BSD license.