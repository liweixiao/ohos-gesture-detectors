package com.almeros.ohos.multitouch.library;

import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.math.BigDecimal;

/**
 * @author Almer Thie (code.almeros.com)
 * Copyright (c) 2013, Almer Thie (code.almeros.com)
 * <p>
 * All rights reserved.
 * <p>
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * <p>
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the distribution.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
public abstract class TwoFingerGestureDetector extends BaseGestureDetector {

    protected float mPrevFingerDiffX;
    protected float mPrevFingerDiffY;
    protected float mCurrFingerDiffX;
    protected float mCurrFingerDiffY;

    public TwoFingerGestureDetector(Context context) {
        super(context);
    }

    @Override
    protected abstract void handleStartProgressEvent(int actionCode, TouchEvent event);

    @Override
    protected abstract void handleInProgressEvent(int actionCode, TouchEvent event);

    protected void updateStateByEvent(TouchEvent curr) {
        super.updateStateByEvent(curr);

        final TouchEvent prev = mPrevEvent;

        // Previous
        final float px0 = prev.getPointerPosition(0).getX();
        final float py0 = prev.getPointerPosition(0).getY();
        final float px1 = prev.getPointerPosition(1).getX();
        final float py1 = prev.getPointerPosition(1).getY();
        BigDecimal bignum1 = new BigDecimal(px1);
        BigDecimal bignum2 = new BigDecimal(px0);
        final float pvx = bignum1.subtract(bignum2).floatValue();
        BigDecimal bignum3 = new BigDecimal(py1);
        BigDecimal bignum4 = new BigDecimal(py0);
        final float pvy = bignum3.subtract(bignum4).floatValue();
        mPrevFingerDiffX = pvx;
        mPrevFingerDiffY = pvy;

        // Current
        final float cx0 = curr.getPointerPosition(0).getX();
        final float cy0 = curr.getPointerPosition(0).getY();
        final float cx1 = curr.getPointerPosition(1).getX();
        final float cy1 = curr.getPointerPosition(1).getY();
        BigDecimal bignum5 = new BigDecimal(cx1);
        BigDecimal bignum6 = new BigDecimal(cx0);
        final float cvx = bignum5.subtract(bignum6).floatValue();
        BigDecimal bignum7 = new BigDecimal(cy1);
        BigDecimal bignum8 = new BigDecimal(cy0);
        final float cvy = bignum7.subtract(bignum8).floatValue();
        mCurrFingerDiffX = cvx;
        mCurrFingerDiffY = cvy;
    }

    /**
     * MotionEvent has no getRawX(int) method; simulate it pending future API approval.
     *
     * @param event
     * @param pointerIndex
     * @return float
     */
    protected static float getRawX(TouchEvent event, int pointerIndex) {
        BigDecimal bignum1 = new BigDecimal(event.getPointerScreenPosition(event.getIndex()).getX());
        BigDecimal bignum2 = new BigDecimal(event.getPointerPosition(event.getIndex()).getX());
        float viewToRaw = bignum1.subtract(bignum2).floatValue();
        if (pointerIndex < event.getPointerCount()) {
            BigDecimal bignum3 = new BigDecimal(event.getPointerPosition(pointerIndex).getX());
            BigDecimal bignum4 = new BigDecimal(viewToRaw);
            return bignum3.add(bignum4).floatValue();
        }
        return 0f;
    }

    /**
     * MotionEvent has no getRawY(int) method; simulate it pending future API approval.
     *
     * @param event event
     * @param pointerIndex pointerIndex
     * @return float
     */
    protected static float getRawY(TouchEvent event, int pointerIndex) {
        BigDecimal bignum1 = new BigDecimal(event.getPointerScreenPosition(event.getIndex()).getY());
        BigDecimal bignum2 = new BigDecimal(event.getPointerPosition(event.getIndex()).getY());
        float viewToRaw = bignum1.subtract(bignum2).floatValue();
        if (pointerIndex < event.getPointerCount()) {
            BigDecimal bignum3 = new BigDecimal(event.getPointerPosition(pointerIndex).getY());
            BigDecimal bignum4 = new BigDecimal(viewToRaw);
            return bignum3.add(bignum4).floatValue();
        }
        return 0f;
    }

    /**
     * Check if we have a sloppy gesture. Sloppy gestures can happen if the edge
     * of the user's hand is touching the screen, for example.
     *
     * @param event
     * @return float
     */
    protected boolean isSloppyGesture(TouchEvent event) {
        final float edgeSlop = 0;
        final float rightSlop = 1080;
        final float bottomSlop = 0;

        final float x0 = event.getPointerScreenPosition(event.getIndex()).getX();
        final float y0 = event.getPointerScreenPosition(event.getIndex()).getY();
        final float x1 = getRawX(event, 1);
        final float y1 = getRawY(event, 1);

        boolean p0sloppy = x0 < edgeSlop || y0 < edgeSlop
                || x0 > rightSlop || y0 > bottomSlop;
        boolean p1sloppy = x1 < edgeSlop || y1 < edgeSlop
                || x1 > rightSlop || y1 > bottomSlop;

        if (p0sloppy && p1sloppy) {
            return true;
        } else if (p0sloppy) {
            return true;
        } else if (p1sloppy) {
            return true;
        }
        return false;
    }

}
